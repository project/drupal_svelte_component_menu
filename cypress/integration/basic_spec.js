const visit = () => {
    cy.visit(Cypress.config().baseUrl);
};

describe('Basic', () => {
    describe('Test if things gets loaded properly', () => {
        beforeEach(visit);
        it('Sample page loads without error', () => {
            cy.contains('button', 'Fetch').should('to.exist');
        });
    });
});
