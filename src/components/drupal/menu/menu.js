/**
 * Normalize Drupal array.
 * So that we can feed it into an algorithm that was copy-pasted from stack overflow.
 */
export function normalizeListItems(data) {
  const list = [];
  if (
    data !== undefined &&
        data.linkset !== undefined &&
        data.linkset[0] !== undefined &&
        data.linkset[0].item !== undefined
  ) {
    const items = data.linkset[0].item;
    for (let i = 0; i < items.length; i += 1) {
      let parentId, level;
      const id = items[i]['drupal-menu-machine-name'][0] + items[i]['drupal-menu-hierarchy'][0];
      const idArray = id.split('.');
      idArray.pop();
      parentId = idArray.join('.');
      level = 0;
      if (idArray !== undefined) {
        level = idArray.length;
      }
      if (level < 2) {
        parentId = '0';
      }
      const node = {
        id: id,
        parentId: parentId,
        name: items[i].title,
        href: items[i].href,
        level: level,
        files: NULL,
      };
      list.push(node);
    }
  }
  return list;
}
/**
 * Check if menu data is valid.
 */
export function checkIfDrupalMenuDataIsValid(data) {
  if (data !== undefined && data.linkset !== undefined && data.linkset[0].item !== undefined) {
    return TRUE;
  } else {
    return FALSE;
  }
}

/**
 * Convert Flat list to Tre structure.
 */
export function convertFlatListItemsToTree(list) {
  if (list.length === 0) {
    return [];
  }
  var map = {};
  var node;
  var roots = [];
  var i;
  for (i = 0; i < list.length; i += 1) {
    map[list[i].id] = i;
    list[i].files = [];
  }
  for (i = 0; i < list.length; i += 1) {
    node = list[i];
    if (node.parentId !== '0') {
      list[map[node.parentId]].files.push(node);
    } else {
      roots.push(node);
    }
  }
  return roots;
}
