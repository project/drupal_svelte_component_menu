# Drupal svelte component menu


## TODO

* [ ] Publish to npm to make it easier to consume component.
* [ ] Test component
* [ ] Auto Publish to npm
* [ ] Storybook Integration
* [ ] Initialize component using url
* [ ] Add example to consume component outside svelte (should be possible after the module is published to npm).

## Setting up

* Run `npm init` (or `yarn init`)

### Available tasks.

Refer to available tasks inside package.json "scripts" for latest.

* `yarn run build` to build the component
* `yarn run autobuild ` to build the component while developing, to build continusly upon source code update.
* `yarn run dev ` to start a local development server for testing and as well as run autobuild.
* `yarn start` to serve example.
* `yarn start --dev` to serve example in dev mode.

## Example

Running `yarn start` should start a local server. The link to view the example should be available in command line output and should look something like  http://localhost:5000. Please note that, in case port 5000 is already busy, it may pick up some other random port and the link would look similar to  http://localhost:57391.
## Consuming components

Your package.json has a `"svelte"` field pointing to `src/index.js`, which allows Svelte apps to import the source code directly, if they are using a bundler plugin like [rollup-plugin-svelte](https://github.com/sveltejs/rollup-plugin-svelte) or [svelte-loader](https://github.com/sveltejs/svelte-loader) (where [`resolve.mainFields`](https://webpack.js.org/configuration/resolve/#resolve-mainfields) in your webpack config includes `"svelte"`). **This is recommended.**

For everyone else, `npm run build` will bundle your component's source code into a plain JavaScript module (`dist/index.mjs`) and a UMD script (`dist/index.js`).